#!/usr/bin/env bash

## Install Guide:
##    https://codelabs.developers.google.com/codelabs/clasp/
##    https://script.google.com/home/usersettings
# sudo npm i inquirer     - g
sudo npm install inquirer                     -g
sudo npm install inquirer-autocomplete-prompt -g
sudo npm install @google/clasp                -g
